export interface INewMessage {
    id: number;
    content: string;
    chatId: number;
    channelId: number;
}