export interface IChannel {
    id: number;
    title: string;
}
