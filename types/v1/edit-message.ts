export interface IEditMessage {
    id: number;
    content: string;
}
