export interface IMessage {
    id: number;
    chatId: number;
    channelId: number;
    content: string;
}
