export interface IEditChannel {
    id: number;
    title: string;
}
